<?php 
//Template name: home
 get_header();?>
<div class="home">
    <div class="parteDeCimaHome">
        <h1>Comes&Bebes</h1>
        <h2>O restaurante para todas as fomes</h1>
    </div>
    <div class="meioHome">
        <h1 class="tituloMeioHome">CONHEÇA NOSSA LOJA</h1>
        <p class="tituloTiposPratos">Tipos de pratos principais</p>
        <div class="categoriasHome">
            <?php 
                $categorias_final = get_link_category_img();
                foreach($categorias_final as $category){
                    if($category['name'] != 'Uncategorized'){ ?>
                            <div class="imagensCategoriasHome">
                                <img id="imagensCategoriaCasa" src="<?php echo $category['img']; ?>" alt="categorias">
                                <div class="linkCategoriasHome"><a href="<?php echo $category['link'] ?>"><?php echo $category['name'] ?></a></div>
                            </div>
                    <?php
                    }
                };
            ?>
        </div>
        <div>
            <p class="textoPratosDia">Pratos do dia de hoje:</p>
            
            <p class="textoDia"><?php
            date_default_timezone_set("America/Sao_Paulo");
            $diaDaSemana = date('l');
            switch ($diaDaSemana) {
            case 'Monday':
                echo "SEGUNDA";
                break;
            case 'Tuesday':
                echo "TERÇA";
                break;
            case 'Wednesday':
                echo "QUARTA";
                break;
            case 'Thursday':
                echo "QUINTA";
                break;
            case 'Friday':
                echo "SEXTA";
                break;
            case 'Saturday':
                echo "SÁBADO";
                break;
            case 'Sunday':
                echo "DOMINGO";
                break;
            default:
                echo "O mundo acabou, pois hoje nao é dia da semana";
            }
        ?></p>
        </div>
        <div class="pratosDiaHome">
                <?php
                $products = wc_get_products([
                    'limit' =>4,
                    'tag' =>date('l'),
                ]);
                $products_formatado = format_products($products);

                foreach($products_formatado as $product) { ?>
                    <div class="pratosDiaHome1">
                        <?php echo $product['img'] ?>
                        <div class="container-infos">
                            <p class="nome-do-dia"><?php echo $product['name']; ?></p>
                            <div class="botaoEPreco">
                                <p class="preco-do-dia">R$<?php echo $product['price']; ?></p>
                                <a href="<?php echo $product['link'] ?>" class="adicionar-carrinho"><img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/adicionar-carrinho.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
    <div class="botaoLoja1">
        <a class="botaoLoja2" href="http://comesbebes.local/shop/"><div class="botaoLoja">Veja outras opções</div></a>
    </div>
    <div class="homeBaixo">
    <h1 class="tituloHome">VISITE NOSSA LOJA FÍSICA</h1>
    <div class="parteDeBaixoHome">
        <div class="parte1Home">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3676.093573598182!2d-42.03803158503492!3d-22.873001485029036!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x971ad575788be1%3A0x292f82d8be77543b!2sShopping%20Park%20Lagos!5e0!3m2!1spt-BR!2sbr!4v1634088008248!5m2!1spt-BR!2sbr" width="250" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>    
            <div class="linhas"><img class="imagensHome" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/endereco.png">Av. Henrique Terra, 1700 - Palmeiras, Cabo Frio - RJ, 28911-320</div>
            <div class="linhas"><img class="imagensHome" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/telefone.png">(22) 99102-7079</div>
        </div>
        <div class="parte2Home">   
            <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/img1slider.png" width="400">
        </div>
    </div>
    </div>
</div>
<?php get_footer();?>
