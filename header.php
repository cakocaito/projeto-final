<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <title>Home</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
    <header>
            <div class="parte1Header">
                <a href="http://comesbebes.local/"><img class="logoHeader" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/Logo.png" alt="logo"></a>
                <div class="lupaNaPesquisa">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/imagens/lupa.png">
                    <input class="pesquisaHeader"type="text">
                </div>
            </div>
            <div class="parte2Header">
                <a href="" class="botaoPedidoHeader">Faça um pedido</a>
                <a><img id="botaoCarrinho" class="imagensHeader" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/carrinho.png"></a>
                <a href="http://comesbebes.local/my-account/"><img class="imagensHeader" src="<?php echo get_stylesheet_directory_uri() ?>/imagens/login.png"></a>
            </div>
        <div class="containerCarrinho hidden"></div>
        <div class="apareceCarrinho hidden">
            <span id="tituloCarrinho">Carrinho</span>
            <span id="fechar">X</span>
            <hr>
            <?php 
                global $woocommerce;
                $items = $woocommerce->cart->get_cart(); 


                foreach($items as $item => $values) { 
                    $_product =  wc_get_product( $values['data']->get_id()); 
                    echo $_product->get_image();
                    echo "<b>".$_product->get_title().'</b>  <br> Quantidade: '.$values['quantity'].'<br>'; 
                    $price = get_post_meta($values['product_id'] , '_price', true);
                    echo "  Preço <b>".$price."</b> <br>";
                    // echo "Link do produto: ".$_product->get_permalink();

                } 

            ?>
            <hr>
            <p id="mostrarTotal">Total do Carrinho: <b><?php echo WC()->cart->get_cart_total(); ?></b></p>
            <button class="comprarCarrinho"><a class="comprarCarrinho1" href="<?php echo wc_get_checkout_url(); ?>">Comprar</a></button>
        </div>
        <script>
            const carrinho = document.querySelector('#botaoCarrinho')
            carrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.apareceCarrinho')
                const containerCarrinho = document.querySelector('.containerCarrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

            const fecharCarrinho = document.querySelector('#fechar')
            fecharCarrinho.addEventListener('click', (e)=>{
                const apareceCarrinho = document.querySelector('.apareceCarrinho')
                const containerCarrinho = document.querySelector('.containerCarrinho')

                apareceCarrinho.classList.toggle("hidden");
                containerCarrinho.classList.toggle("hidden");
            })

        </script>
    </header>

